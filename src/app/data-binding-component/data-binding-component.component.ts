import { Component } from '@angular/core';

@Component({
  selector: 'app-data-binding-component',
  templateUrl: './data-binding-component.component.html',
  styleUrls: ['./data-binding-component.component.css']
})
export class DataBindingComponentComponent {
   title='Data Binding Component';
   
   book:any ={
     title:"String interpolation",
     author:"Sanjana"
   }
   
   name='sanjana';

   isDisable: boolean= false;


}
